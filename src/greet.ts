/**
 * Created by mare on 2016/12/20.
 */

export function sayHello(name: string) {
    return `Hello from ${name}`;
}
